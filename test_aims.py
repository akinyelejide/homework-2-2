from nose.tools import assert_equal

import aims
def test_ints():
    numbers = [45,45]
    obs = aims.std(numbers)
    exp = 8
    assert_equal(obs,exp)

def test_double():
    numbers = [-4, -5]
    obs = aims.std(numbers)
    exp = 0.5
    assert_equal(obs,exp)

def test_negaints():
    numbers = [-2, -2]
    obs = aims.std(numbers)
    exp = 3
    assert_equal(obs, exp)
def test_avg1():
    files = ['data/bert/audioresult-00215']
    abs = aims.avg_range(files)
    exp = 5
    assert_equal(abs, exp)
